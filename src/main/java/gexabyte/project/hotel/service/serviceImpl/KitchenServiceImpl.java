package gexabyte.project.hotel.service.serviceImpl;

import gexabyte.project.hotel.dto.KitchenDTO;
import gexabyte.project.hotel.entity.Hotel;
import gexabyte.project.hotel.entity.Kitchen;
import gexabyte.project.hotel.exception.CustomBadRequestException;
import gexabyte.project.hotel.exception.CustomNotFoundException;
import gexabyte.project.hotel.repository.HotelRepository;
import gexabyte.project.hotel.repository.KitchenRepository;
import gexabyte.project.hotel.repository.UserRepository;
import gexabyte.project.hotel.security.jwt.JwtUser;
import gexabyte.project.hotel.service.KitchenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class KitchenServiceImpl implements KitchenService {

    private final HotelRepository hotelRepository;
    private final KitchenRepository kitchenRepository;
    private final UserRepository userRepository;

    @Autowired
    public KitchenServiceImpl(HotelRepository hotelRepository,
                              KitchenRepository kitchenRepository,
                              UserRepository userRepository) {
        this.hotelRepository = hotelRepository;
        this.kitchenRepository = kitchenRepository;
        this.userRepository = userRepository;
    }

    @Override
    public ResponseEntity<?> createKitchen(KitchenDTO kitchenDTO) {
        Hotel hotelKitchen = hotelRepository.findById(kitchenDTO.getHotelId())
                .orElseThrow(() -> new CustomNotFoundException("Hotel not found"));
        Kitchen kitchen = new Kitchen();
        kitchen.setFoodName(kitchenDTO.getFoodName());
        kitchen.setPrice(kitchenDTO.getPrice());
        kitchen.setHotels(hotelKitchen);

        kitchenRepository.save(kitchen);

        log.info("IN create - kitchen: {} successfully created", kitchen);

        return new ResponseEntity<>(HttpStatus.OK);

    }

    @Override
    public ResponseEntity<?> updateKitchen(KitchenDTO kitchenDTO, Long id) {
        Kitchen updateKitchen = kitchenRepository.findById(kitchenDTO.getHotelId())
                .orElseThrow(() -> new CustomNotFoundException("Kitchen not found"));
        Hotel hotelKitchen = hotelRepository.findById(updateKitchen.getHotels().getId())
                .orElseThrow(() -> new CustomNotFoundException("Hotel not found"));

        updateKitchen.setFoodName(kitchenDTO.getFoodName());
        updateKitchen.setPrice(kitchenDTO.getPrice());
        updateKitchen.setHotels(hotelKitchen);

        kitchenRepository.save(updateKitchen);

        log.info("IN create - kitchen: {} successfully created", updateKitchen);

        return new ResponseEntity<>(HttpStatus.OK);

    }

    @Override
    public void deleteKitchen(Long id) {

        if(!kitchenRepository.existsById(id)){
            throw new CustomNotFoundException("Kitchen not found");
        }

        kitchenRepository.deleteById(id);
        log.info("IN create - booking: {} successfully deleted", id);
    }

    @Override
    public ResponseEntity<?> createOrder(KitchenDTO kitchenDTO) {
        Hotel hotelKitchen = hotelRepository.findById(kitchenDTO.getHotelId())
                .orElseThrow(() -> new CustomNotFoundException("Hotel not found"));

        JwtUser principal = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Kitchen kitchen = new Kitchen();
        kitchen.setFoodName(kitchenDTO.getFoodName());
        kitchen.setPrice(kitchenDTO.getPrice());
        kitchen.setHotels(hotelKitchen);

        principal.getUser().setMoney(principal.getUser().getMoney().subtract(kitchen.getPrice()));

        userRepository.save(principal.getUser());
        kitchenRepository.save(kitchen);

        log.info("IN create - kitchen: {} successfully created", kitchen);

        return new ResponseEntity<>(HttpStatus.OK);

    }
}
