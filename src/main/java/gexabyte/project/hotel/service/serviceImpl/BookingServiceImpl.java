package gexabyte.project.hotel.service.serviceImpl;

import gexabyte.project.hotel.dto.BookingDTO;
import gexabyte.project.hotel.entity.Booking;
import gexabyte.project.hotel.entity.Hotel;
import gexabyte.project.hotel.entity.Room;
import gexabyte.project.hotel.entity.Type;
import gexabyte.project.hotel.exception.CustomBadRequestException;
import gexabyte.project.hotel.exception.CustomNotFoundException;
import gexabyte.project.hotel.repository.*;
import gexabyte.project.hotel.security.jwt.JwtUser;
import gexabyte.project.hotel.service.BookingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class BookingServiceImpl implements BookingService {
    private final HotelRepository hotelRepository;
    private final BookingRepository bookingRepository;
    private final TypeRepository typeRepository;
    private final RoomRepository roomRepository;
    private final UserRepository userRepository;

    @Autowired
    public BookingServiceImpl(
            HotelRepository hotelRepository,
            BookingRepository bookingRepository,
            TypeRepository typeRepository,
            RoomRepository roomRepository,
            UserRepository userRepository) {
        this.hotelRepository = hotelRepository;
        this.bookingRepository = bookingRepository;
        this.typeRepository = typeRepository;
        this.roomRepository = roomRepository;
        this.userRepository = userRepository;
    }

    @Override
    public ResponseEntity<?> createBooking(BookingDTO bookingDTO) {
        String typeName = bookingDTO.getRoomTypes();
        Long hotelId = bookingDTO.getHotelId();

        Booking booking = new Booking();

        Type type = typeRepository.findByName(typeName);

        Hotel hotel = hotelRepository.findById(hotelId)
                .orElseThrow(() -> new CustomNotFoundException("Hotel not found"));

        JwtUser principal = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if(!typeRepository.existsByName(typeName)){
            throw new CustomNotFoundException("Room type not found");
        }

        Room room = roomRepository.findByTypes_Name(typeName);

        principal.getUser().setMoney(principal.getUser().getMoney().subtract(room.getPrice()));

        booking.setTypes(type);
        booking.setHotels(hotel);
        booking.setUsers(principal.getUser());

        userRepository.save(principal.getUser());
        bookingRepository.save(booking);

        log.info("IN create - booking: {} successfully created", booking);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> updateBooking(BookingDTO bookingDTO, Long id) {

        if(!bookingRepository.existsById(id)){
            throw new CustomNotFoundException("Booking not found");
        }

        Long hotelId = bookingDTO.getHotelId();
        String typeName = bookingDTO.getRoomTypes();

        Booking updateBookingDTO = findBookingById(id);

        Hotel hotel = hotelRepository.findById(hotelId)
                .orElseThrow(() -> new CustomNotFoundException("Hotel not found"));

        if(!typeRepository.existsByName(typeName)){
            throw new CustomNotFoundException("Room type not found");
        }

        Type roomTypes = typeRepository.findByName(typeName);

        JwtUser principal = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        updateBookingDTO.setUsers(principal.getUser());
        updateBookingDTO.setHotels(hotel);
        updateBookingDTO.setTypes(roomTypes);

        bookingRepository.save(updateBookingDTO);
        log.debug("IN updateBooking - booking:{} successfully updated", updateBookingDTO);
        return new ResponseEntity<>("Booking update successfully" ,HttpStatus.OK);
    }


    @Override
    public Booking findBookingById(Long id){
        Booking bookingId = bookingRepository.findById(id)
                .orElseThrow(() -> new CustomNotFoundException("Booking not found"));
        log.info("IN findBookingById - booking found by id: {}", bookingId);
        return bookingId;
    }

    @Override
    public void deleteBooking(Long id) {

        if(!bookingRepository.existsById(id)){
            throw new CustomNotFoundException("Booking not found");
        }

        bookingRepository.deleteById(id);
        log.info("IN deleteBooking - booking: {} successfully deleted", id);
    }
}
