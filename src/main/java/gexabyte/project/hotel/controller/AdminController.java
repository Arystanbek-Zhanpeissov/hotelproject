package gexabyte.project.hotel.controller;


import gexabyte.project.hotel.dto.HotelDTO;
import gexabyte.project.hotel.dto.KitchenDTO;
import gexabyte.project.hotel.dto.RoomDTO;
import gexabyte.project.hotel.dto.UserDTO;
import gexabyte.project.hotel.service.HotelService;
import gexabyte.project.hotel.service.KitchenService;
import gexabyte.project.hotel.service.RoomService;
import gexabyte.project.hotel.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping(value = "/api/v1/admin/")
public class AdminController {

    private final UserService userService;
    private final HotelService hotelService;
    private final RoomService roomService;
    private final KitchenService kitchenService;

    @Autowired
    public AdminController(UserService userService,
                           HotelService hotelService,
                           RoomService roomService,
                           KitchenService kitchenService) {
        this.userService = userService;
        this.hotelService = hotelService;
        this.roomService = roomService;
        this.kitchenService = kitchenService;
    }

    @PostMapping("/create")
    public ResponseEntity<?> createAdmin(@RequestBody UserDTO userDTO) {
        userService.createAdmin(userDTO);
        log.info("Admin created");
        return new ResponseEntity<>("admin created", HttpStatus.CREATED);
    }

    @PostMapping("/user/update/{id}")
    public ResponseEntity<?> updateUser(@RequestBody UserDTO userDTO,
                                        @PathVariable(name = "id") Long id) {
        userService.updateUser(userDTO, id);
        log.info("User updated by id ={}", id);
        return new ResponseEntity<>("User updated", HttpStatus.OK);
    }

    @PostMapping("/user/delete/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable(name = "id") Long id) {
        userService.deleteUser(id);
        log.info("User deleted by id = {}", id);
        return new ResponseEntity<>("User deleted", HttpStatus.OK);
    }

    @PostMapping("/hotel/create")
    public ResponseEntity<?> createHotel(@RequestBody HotelDTO hotelDTO) {
        hotelService.createHotel(hotelDTO);
        log.info("Hotel created");
        return new ResponseEntity<>("Hotel created", HttpStatus.CREATED);
    }

    @PostMapping("/hotel/update/{id}")
    public ResponseEntity<?> updateHotel(@RequestBody HotelDTO hotelDTO,
                                         @PathVariable(name = "id") Long id) {
        hotelService.updateHotel(hotelDTO, id);
        log.info("Hotel updated by id ={}", id);
        return new ResponseEntity<>("Hotel updated", HttpStatus.OK);
    }

    @PostMapping(value = "/hotel/delete/{id}")
    public ResponseEntity<?> deleteHotelById(@PathVariable(name = "id") Long id) {
        hotelService.deleteHotel(id);
        log.info("Hotel deleted by id ={}", id);
        return new ResponseEntity<>("Hotel deleted", HttpStatus.OK);
    }


    @PostMapping("/room/create")
    public ResponseEntity<?> createRoom(@RequestBody RoomDTO roomDTO) {
        roomService.createRoom(roomDTO);
        log.info("Room created");
        return ResponseEntity.ok("Room created");
    }

    @PostMapping("/room/update/{id}")
    public ResponseEntity<?> updateRoom(@RequestBody RoomDTO roomDTO,
                                        @PathVariable(name = "id") Long id) {
        roomService.updateRoom(roomDTO, id);
        log.info("Room updated by id ={}", id);
        return ResponseEntity.ok("Room updated");
    }

    @PostMapping(value = "/room/delete/{id}")
    public ResponseEntity<?> deleteRoomById(@PathVariable(name = "id") Long id) {
        roomService.deleteRoom(id);
        log.info("Room deleted by id ={}", id);
        return ResponseEntity.ok("Room deleted");
    }

    @PostMapping("/kitchen/create")
    public ResponseEntity<?> createKitchen(@RequestBody KitchenDTO kitchenDTO) {
        kitchenService.createKitchen(kitchenDTO);
        log.info("Kitchen created");
        return ResponseEntity.ok("Kitchen created");
    }

    @PostMapping("/kitchen/create/order")
    public ResponseEntity<?> createOrder(@RequestBody KitchenDTO kitchenDTO) {
        kitchenService.createOrder(kitchenDTO);
        log.info("Order created");
        return ResponseEntity.ok("Order created");
    }

    @PostMapping("/kitchen/update/{id}")
    public ResponseEntity<?> updateKitchen(@RequestBody KitchenDTO kitchenDTO,
                                           @PathVariable(name = "id") Long id) {
        kitchenService.updateKitchen(kitchenDTO, id);
        log.info("Kitchen updated by id ={}", id);
        return ResponseEntity.ok("Kitchen updated");
    }

    @PostMapping(value = "/kitchen/delete/{id}")
    public ResponseEntity<?> deleteKitchenById(@PathVariable(name = "id") Long id) {
        kitchenService.deleteKitchen(id);
        log.info("Kitchen deleted by id ={}", id);
        return ResponseEntity.ok("Kitchen deleted");
    }
}
