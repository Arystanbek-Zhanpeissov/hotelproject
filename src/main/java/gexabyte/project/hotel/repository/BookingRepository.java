package gexabyte.project.hotel.repository;

import gexabyte.project.hotel.entity.Booking;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookingRepository extends JpaRepository<Booking, Long> {
    boolean existsById(Long id);
}
