package gexabyte.project.hotel.service.serviceImpl;

import gexabyte.project.hotel.dto.UserDTO;
import gexabyte.project.hotel.entity.Role;
import gexabyte.project.hotel.entity.User;
import gexabyte.project.hotel.exception.CustomBadRequestException;
import gexabyte.project.hotel.exception.CustomNotFoundException;
import gexabyte.project.hotel.repository.RoleRepository;
import gexabyte.project.hotel.repository.UserRepository;
import gexabyte.project.hotel.service.UserService;
import gexabyte.project.hotel.validation.Validation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final Validation validation;


    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           RoleRepository roleRepository,
                           BCryptPasswordEncoder passwordEncoder,
                           Validation validation) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.validation = validation;
    }

    @Override
    public ResponseEntity<?> createUser(UserDTO userDTO) {

        Role roleUser = roleRepository.findById(1L)
                .orElseThrow(() -> new CustomNotFoundException("Role not found"));
        User user = new User();

        validation.passwordValid(userDTO.getPassword(), userDTO.getConfirmPassword());
        validation.moneyValid(userDTO.getMoney());
        validation.emailValid(userDTO.getEmail());

        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setMoney(userDTO.getMoney());
        user.setEmail(userDTO.getEmail());
        user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        user.setRoles(roleUser);
        userRepository.save(user);

        log.info("IN register - user: {} successfully registered", user);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<?> createAdmin(UserDTO userDTO) {
        Role roleUser = roleRepository.findById(2L)
                .orElseThrow(() -> new CustomBadRequestException("Role not found"));

        User user = new User();

        validation.passwordValid(userDTO.getPassword(), userDTO.getConfirmPassword());
        validation.moneyValid(userDTO.getMoney());
        validation.emailValid(userDTO.getEmail());

        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setMoney(userDTO.getMoney());
        user.setEmail(userDTO.getEmail());
        user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        user.setRoles(roleUser);
        userRepository.save(user);

        log.info("IN register - admin:{} successfully registered", user);

        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<?> updateUser(UserDTO userDTO, Long id) {

        User updateUser = userRepository.findById(id)
                .orElseThrow(() -> new CustomNotFoundException("User not found"));

        Role roleUser = roleRepository.findById(1L)
                .orElseThrow(() -> new CustomNotFoundException("Role not found"));
        updateUser.setRoles(roleUser);

        updateUser.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        userRepository.save(updateUser);

        log.info("IN register - user:{} successfully registered", updateUser);

        return new ResponseEntity<>(updateUser, HttpStatus.OK);
    }


    @Override
    public User findUserById(Long id) {
        User userId = userRepository.findById(id)
                .orElseThrow(() -> new CustomNotFoundException("User not found"));

        log.info("IN findUserById - user found by id: {}", userId);
        return userId;
    }

    @Override
    public User findUserByEmail(String email) {

        if(!userRepository.existsByEmail(email)){
            throw new CustomNotFoundException("User not found");
        }

        User userEmail = userRepository.findByEmail(email);

        log.info("IN findUserByEmail - user found by id:{}", userEmail);
        return userEmail;

    }

    @Override
    public List<User> allUsers() {
        List<User> allUsers = userRepository.findAll();
        log.info("IN getAll - {} users found", allUsers.size());
        return allUsers;
    }

    @Override
    public void deleteUser(Long id) {

        if(!userRepository.existsById(id)){
            throw new CustomNotFoundException("User not found");
        }

        userRepository.deleteById(id);
        log.info("IN delete - user with id: {} successfully deleted", id);
    }
}
