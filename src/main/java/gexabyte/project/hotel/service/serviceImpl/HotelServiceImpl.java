package gexabyte.project.hotel.service.serviceImpl;

import gexabyte.project.hotel.dto.HotelDTO;
import gexabyte.project.hotel.entity.Hotel;
import gexabyte.project.hotel.exception.CustomBadRequestException;
import gexabyte.project.hotel.exception.CustomNotFoundException;
import gexabyte.project.hotel.repository.HotelRepository;
import gexabyte.project.hotel.service.HotelService;
import gexabyte.project.hotel.validation.Validation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class HotelServiceImpl implements HotelService {

    private final HotelRepository hotelRepository;
    private final Validation validation;

    @Autowired
    public HotelServiceImpl(HotelRepository hotelRepository, Validation validation) {
        this.hotelRepository = hotelRepository;
        this.validation = validation;
    }

    @Override
    public ResponseEntity<?> createHotel(HotelDTO hotelDTO) {

        Hotel hotel = new Hotel();

        validation.starsNumberValid(hotelDTO.getStarsNumber());

        hotel.setName(hotelDTO.getName());
        hotel.setStarsNumber(hotelDTO.getStarsNumber());

        hotelRepository.save(hotel);

        log.info("IN create - hotel: {} successfully created", hotel);

        return new ResponseEntity<>(HttpStatus.OK);
    }
    @Override
    public ResponseEntity<?> updateHotel(HotelDTO hotelDTO, Long id) {

        Hotel updateHotel = findHotelById(id);

        validation.starsNumberValid(hotelDTO.getStarsNumber());

        updateHotel.setName(hotelDTO.getName());
        updateHotel.setStarsNumber(hotelDTO.getStarsNumber());

        hotelRepository.save(updateHotel);

        log.info("IN updateHotel - hotel: {} successfully updated", updateHotel);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public Hotel findHotelById(Long id){

        Hotel hotelId = hotelRepository.findById(id)
                .orElseThrow(() -> new CustomNotFoundException("Hotel not found"));

        log.info("IN findBookingById - booking found by id:{}", hotelId);
        return hotelId;
    }
    @Override
    public void deleteHotel(Long id) {

        if(!hotelRepository.existsById(id)){
            throw new CustomNotFoundException("Hotel not found");
        }

        hotelRepository.deleteById(id);
        log.info("IN create - booking:{} successfully deleted", id);
    }
}
