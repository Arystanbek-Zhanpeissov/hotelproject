package gexabyte.project.hotel.service.serviceImpl;

import gexabyte.project.hotel.dto.RoomDTO;
import gexabyte.project.hotel.entity.Hotel;
import gexabyte.project.hotel.entity.Room;
import gexabyte.project.hotel.entity.Type;
import gexabyte.project.hotel.exception.CustomBadRequestException;
import gexabyte.project.hotel.exception.CustomNotFoundException;
import gexabyte.project.hotel.repository.HotelRepository;
import gexabyte.project.hotel.repository.RoomRepository;
import gexabyte.project.hotel.repository.TypeRepository;
import gexabyte.project.hotel.service.RoomService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class RoomServiceImpl implements RoomService {
    private final RoomRepository roomRepository;
    private final TypeRepository typeRepository;
    private final HotelRepository hotelRepository;

    @Autowired
    public RoomServiceImpl(RoomRepository roomRepository, TypeRepository typeRepository,
                           HotelRepository hotelRepository) {
        this.roomRepository = roomRepository;
        this.typeRepository = typeRepository;
        this.hotelRepository = hotelRepository;
    }

    @Override
    public ResponseEntity<?> createRoom(RoomDTO roomDTO) {
        Type typeRoom = typeRepository.findById(roomDTO.getTypeId())
                .orElseThrow(() -> new CustomNotFoundException("Type not found"));
        Hotel hotelRoom = hotelRepository.findById(roomDTO.getHotelId())
                .orElseThrow(() -> new CustomNotFoundException("Hotel not found"));

        Room room = new Room();

        room.setTypes(typeRoom);
        room.setHotels(hotelRoom);
        room.setPrice(roomDTO.getPrice());

        roomRepository.save(room);

        log.info("IN create - room: {} successfully created", room);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> updateRoom(RoomDTO roomDTO, Long id) {
        Room updateRoom = roomRepository.findById(id)
                .orElseThrow(() -> new CustomNotFoundException("Room not found"));
        Type typeRoom = typeRepository.findById(roomDTO.getTypeId())
                .orElseThrow(() -> new CustomNotFoundException("Type not found"));
        Hotel hotelRoom = hotelRepository.findById(roomDTO.getHotelId())
                .orElseThrow(() -> new CustomNotFoundException("Hotel not found"));

        updateRoom.setTypes(typeRoom);
        updateRoom.setHotels(hotelRoom);
        updateRoom.setPrice(roomDTO.getPrice());

        roomRepository.save(updateRoom);

        log.info("IN update - room: {} successfully created", updateRoom);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public void deleteRoom(Long id) {
        if(!roomRepository.existsById(id)){
            throw new CustomNotFoundException("Room not found");
        }
        roomRepository.deleteById(id);
        log.info("IN create - booking: {} successfully deleted", id);
    }

}
