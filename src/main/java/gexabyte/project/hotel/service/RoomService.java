package gexabyte.project.hotel.service;

import gexabyte.project.hotel.dto.RoomDTO;
import org.springframework.http.ResponseEntity;

public interface RoomService {
    ResponseEntity<?> createRoom(RoomDTO roomDTO);

    ResponseEntity<?> updateRoom(RoomDTO roomDTO, Long id);

    void deleteRoom(Long id);
}
