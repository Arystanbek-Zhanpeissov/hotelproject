package gexabyte.project.hotel.validation;

import gexabyte.project.hotel.exception.CustomBadRequestException;
import gexabyte.project.hotel.repository.UserRepository;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class Validation {


    private final UserRepository userRepository;

    public Validation(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public boolean passwordValid(String password, String confirmPassword){
        if(!password.equals(confirmPassword)){
            throw new CustomBadRequestException("Passwords not equal");
        }
        return true;
    }

    public boolean moneyValid(BigDecimal money){
        if(money.compareTo(BigDecimal.ZERO) < 0){
            throw new CustomBadRequestException("Money must be greater then 0");
        }
        return true;
    }
    public boolean emailValid(String email) {
        String valid = "^[A-Za-z0-9+_.-]+@(.+)$";
        Pattern pattern = Pattern.compile(valid);
        Matcher matcher = pattern.matcher(email);
        if(!matcher.matches()){
            throw new CustomBadRequestException("Email in invalid");
        }
        if(userRepository.existsByEmail(email)){
            throw new CustomBadRequestException("Enter different email");
        }
        return true;
    }

    public boolean starsNumberValid(int number){
        if(1 > number){
            throw new CustomBadRequestException("Enter stars number greater or equal 1");
        }
        if(number > 5){
            throw new CustomBadRequestException("Enter stars number less or equal 5");
        }
        return true;
    }
}
