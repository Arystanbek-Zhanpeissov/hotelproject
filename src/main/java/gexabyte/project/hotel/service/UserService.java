package gexabyte.project.hotel.service;

import gexabyte.project.hotel.dto.UserDTO;
import gexabyte.project.hotel.entity.User;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UserService {

    ResponseEntity<?> createUser(UserDTO userDTO);

    ResponseEntity<?> createAdmin(UserDTO userDTO);

    ResponseEntity<?> updateUser(UserDTO userDTO, Long id);

    User findUserById(Long id);

    User findUserByEmail(String email);

    List<User> allUsers();

    void deleteUser(Long id);
}
