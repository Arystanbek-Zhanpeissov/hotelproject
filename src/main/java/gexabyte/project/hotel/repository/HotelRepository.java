package gexabyte.project.hotel.repository;

import gexabyte.project.hotel.entity.Hotel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface HotelRepository extends JpaRepository<Hotel, Long> {
    Optional<Hotel> findById(Long id);
    boolean existsById(Long id);
}
