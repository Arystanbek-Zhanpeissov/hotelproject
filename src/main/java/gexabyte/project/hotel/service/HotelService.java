package gexabyte.project.hotel.service;

import gexabyte.project.hotel.dto.HotelDTO;
import gexabyte.project.hotel.entity.Hotel;
import org.springframework.http.ResponseEntity;

public interface HotelService {

    ResponseEntity<?> createHotel(HotelDTO hotelDTO);

    ResponseEntity<?> updateHotel(HotelDTO hotelDTO, Long id);

    Hotel findHotelById(Long id);

    void deleteHotel(Long id);
}
