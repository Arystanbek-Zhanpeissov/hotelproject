package gexabyte.project.hotel.repository;

import gexabyte.project.hotel.entity.Type;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TypeRepository extends JpaRepository<Type, Long> {
    Type findByName(String name);
    Optional<Type> findById(Long id);
    boolean existsByName(String name);
}
