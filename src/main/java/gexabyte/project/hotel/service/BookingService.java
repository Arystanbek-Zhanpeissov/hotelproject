package gexabyte.project.hotel.service;

import gexabyte.project.hotel.dto.BookingDTO;
import gexabyte.project.hotel.entity.Booking;
import org.springframework.http.ResponseEntity;

public interface BookingService {

    ResponseEntity<?> createBooking(BookingDTO bookingDTO);

    ResponseEntity<?> updateBooking(BookingDTO bookingDTO, Long id);

    Booking findBookingById(Long id);

    void deleteBooking(Long id);
}
