package gexabyte.project.hotel.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class CustomNotFoundException extends NullPointerException{
    public CustomNotFoundException() {
    }

    public CustomNotFoundException(String s) {
        super(s);
    }
}
