package gexabyte.project.hotel.security.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import gexabyte.project.hotel.exception.JwtAuthenticationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class JwtTokenFilter extends GenericFilterBean {

    private final JwtTokenProvider jwtTokenProvider;

    public JwtTokenFilter(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain)
            throws IOException, ServletException{
            try{
                String token = jwtTokenProvider.resolveToken((HttpServletRequest) req);
                    if (token != null && jwtTokenProvider.validateToken(token)) {
                        Authentication auth = jwtTokenProvider.getAuthentication(token);
                        if (auth != null) {
                            SecurityContextHolder.getContext().setAuthentication(auth);
                        }
                    }
                    filterChain.doFilter(req, res);
            } catch (JwtAuthenticationException e) {
                HttpServletResponse response = (HttpServletResponse) res;
                Map<Object, Object> error = new HashMap<>();
                response.setHeader("Custom-Header", "Token");
                response.setStatus(401);

                error.put("timestamp", Calendar.getInstance().getTime().toString());
                error.put("status", HttpStatus.UNAUTHORIZED.value());
                error.put("error", "Unautorized");
                error.put("message", "Invalid or expired token");
                error.put("path: ", ((HttpServletRequest) req).getRequestURL().toString());

                OutputStream out = response.getOutputStream();
                ObjectMapper mapper = new ObjectMapper();
                mapper.writeValue(out, error);
                out.flush();
                out.close();
            }
    }
}

