/*Role types*/
Insert into roles(name) values ('ROLE_USER');
Insert into roles(name) values ('ROLE_ADMIN');

/*Room types*/
Insert into types(name) values ('STANDARD');
Insert into types(name) values ('LUX');

/*Admin account*/
/*Email admin@mail.com*/
/*Password admin*/
Insert into users(first_name, last_name, email, money, password) values ('admin', 'admin', 'admin@mail.com', 5000, '$2a$10$5egCVWNbzdrWt9PShzUET.frsF1bg28gjIzNI85.wg1yT92QhEDu6');
Insert into users_roles(user_id, role_id) values((select id from users where email = 'admin@mail.com'), 2);

/*User account*/
/*Email user@mail.com*/
/*Password user*/
Insert into users(first_name, last_name, email, money, password) values ('user', 'user', 'user@mail.com', 5000, '$2a$10$0IwP6f/7LeAqL5fvGi/rQuTiAg22S4WO07OrQJgZ.8Gkt3llFDZTa');
Insert into users_roles(user_id, role_id) values((select id from users where email = 'user@mail.com'), 1);

/*Create Hotel*/
Insert into hotels(name, number_of_stars) values ('Hotel1', 5);

/*Create room*/
Insert into rooms(price, hotel_id) values(500, 1);
Insert into rooms_types(room_id, type_id) values(1,1);
