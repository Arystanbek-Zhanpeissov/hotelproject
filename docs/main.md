## API documentation for hotel application

## Main controller

### Table of contents

- [Login](#login)
- [Registration](#registration)
- [Find user by ID](#find-user-by-id)

### Login

#### Description
Login user

#### HTTP request
````
POST /api/v1/main/login

Content-Type: application/json
Required token: no
````

#### HTTP body request params
````
{
    "email": //String
    "password": //String
}
````

#### Sample body request
````
{
    "username": user@mail.com
    "password": user
}
````

##### Sample server response
````
    200 OK
````

#### Errors
````  
    "timestamp": "2020-12-09T14:27:00.836525600",
    "status": 401,
    "error": "Unauthorized",
    "message": "Invalid username or password",
    "path": "/api/v1/main/login"
````

### Registration

#### Description
Register user

#### HTTP request
````
POST /api/v1/main/registration

Content-Type: application/json
Required token: no
````

#### HTTP body request params
````
{
    "firstName": //String
    "lastName": //String
    "email": //String
    "money": //int
    "password": //String
    "confirmPassword": //String
}
````

#### Sample body request
````
{
    "firstName":"user",
    "lastName":"user",
    "email":"user@mail.com",
    "money":5000,
    "password":"user",
    "confirmPassword":"user"
}
````

##### Sample server response
````
    201 Created

````

#### Errors
````
    "timestamp": "2020-12-05T14:09:50.220546500",
    "status": 400,
    "error": "Bad Request",
    "message": "Email in invalid",
    "path": "/api/v1/main/registration"

    "timestamp": "2020-12-05T14:11:17.159078200",
    "status": 400,
    "error": "Bad Request",
    "message": "Money must be greater then 0",
    "path": "/api/v1/main/registration"

    "timestamp": "2020-12-05T14:11:41.726056700",
    "status": 400,
    "error": "Bad Request",
    "message": "Passwords not equal",
    "path": "/api/v1/main/registration"

    "timestamp": "2020-12-07T13:16:17.612828700",
    "status": 400,
    "error": "Bad Request",
    "message": "Enter different email",
    "path": "/api/v1/main/registration"
````

### Find user by ID

#### Description
Find user by ID

#### HTTP request
````
GET /api/v1/main/{id}

Content-Type: application/json
Required token: yes
````

#### HTTP request params
````
    /api/v1/main/1
````

#### Sample body response
````
200 OK
{
    "firstName":"user",
    "lastName":"user",
    "email":"user@mail.com",
    "money":5000,
    "password":"user",
    "confirmPassword":"user"
}
````

#### Errors
````
    "timestamp": "2020-12-10T11:24:35.851710100",
    "status": 404,
    "error": "Not Found",
    "message": "User not found",
    "path": "/api/v1/main/{id}"
````