package gexabyte.project.hotel.repository;

import gexabyte.project.hotel.entity.Kitchen;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KitchenRepository extends JpaRepository<Kitchen, Long> {
    boolean existsById(Long id);
}
