package gexabyte.project.hotel.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KitchenDTO {
    private String foodName;
    private BigDecimal price;
    private Long hotelId;
}
