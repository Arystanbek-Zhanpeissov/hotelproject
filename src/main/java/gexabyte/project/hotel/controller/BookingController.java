package gexabyte.project.hotel.controller;

import gexabyte.project.hotel.dto.BookingDTO;
import gexabyte.project.hotel.repository.BookingRepository;
import gexabyte.project.hotel.service.BookingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "/api/v1/users/booking")
public class BookingController {

    private final BookingService bookingService;
    private final BookingRepository bookingRepository;

    @Autowired
    public BookingController(BookingService bookingService,
                             BookingRepository bookingRepository) {
        this.bookingService = bookingService;
        this.bookingRepository = bookingRepository;
    }

    @PostMapping("/create")
    public ResponseEntity<?> booking(@RequestBody BookingDTO bookingDTO) {
        bookingService.createBooking(bookingDTO);
        log.info("Booking created");
        return new ResponseEntity<>("Booking created", HttpStatus.CREATED);
    }

    @PostMapping(value = "/update/{id}")
    public ResponseEntity<?> updateBooking(@RequestBody BookingDTO bookingDTO,
                                           @PathVariable(name = "id") Long id) {
        bookingService.updateBooking(bookingDTO, id);
        log.info("Booking updated by id ={}", id);
        return ResponseEntity.ok("Booking updated");
    }

    @PostMapping(value = "/delete/{id}")
    public ResponseEntity<?> deleteBookingById(@PathVariable(name = "id") Long id) {
        bookingService.deleteBooking(id);
        log.info("Booking deleted by id ={}", id);
        return ResponseEntity.ok("Booking deleted");
    }
}
