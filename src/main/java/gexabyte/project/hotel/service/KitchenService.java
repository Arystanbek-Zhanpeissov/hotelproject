package gexabyte.project.hotel.service;

import gexabyte.project.hotel.dto.KitchenDTO;
import org.springframework.http.ResponseEntity;

public interface KitchenService {
    ResponseEntity<?> createKitchen(KitchenDTO kitchenDTO);

    ResponseEntity<?> updateKitchen(KitchenDTO kitchenDTO, Long id);

    void deleteKitchen(Long id);

    ResponseEntity<?> createOrder(KitchenDTO kitchenDTO);
}
