## API documentation for hotel application

## Booking controller

### Table of contents
- [Create Booking](#create-booking)
- [Update Booking by ID](#update-booking)
- [Delete Booking by ID](#delete-booking)

### Create Booking

#### Description
Create Booking

#### HTTP request
````
POST /api/v1/users/booking/create

Content-Type: application/json
Required token: yes
````

#### HTTP body request params
````
{
    "hotelId": //int,
    "roomTypes": //String
}
````

#### Sample body request
````
{
    "hotelId": 1,
    "roomTypes": "STANDARD"
}
````

#### Sample Service response
````
    200 OK
````

#### Errors
````
    "timestamp": "2020-12-05T14:50:39.258250600",
    "status": 404,
    "error": "Not Found",
    "message": "Hotel not found",
    "path": "/api/v1/users/booking/create"

    "timestamp": "2020-12-05T15:04:44.037232100",
    "status": 401,
    "error": "Unauthorized",
    "message": "Invalid or expired token",
    "path": "/api/v1 /users/booking/create"
````

### Update Booking

#### Description
Update Booking

#### HTTP request
````
POST /api/v1/users/booking/update/{id}

Content-Type: application/json
Required token: yes
````

#### HTTP body request params
````
{
    "hotelId": //int,
    "roomTypes": //String
}
````

#### Sample body request
````
{
    "hotelId": 1,
    "roomTypes": "STANDARD"
}
````

#### Sample server response
````
    200 OK
````

#### Errors
````
    "timestamp": "2020-12-05T14:50:39.258250600",
    "status": 404,
    "error": "Not Found",
    "message": "Booking not found",
    "path": "/api/v1/users/booking/update/{id}"

    "timestamp": "2020-12-05T14:50:39.258250600",
    "status": 404,
    "error": "Not Found",
    "message": "Hotel not found",
    "path": "/api/v1/users/booking/update/{id}"

    "timestamp": "2020-12-09T15:07:00.875680200",
    "status": 404,
    "error": "Not Found",
    "message": "Room type not found",
    "path": "/api/v1/users/booking/update/{id}"
    
    "timestamp": "2020-12-05T15:04:44.037232100",
    "status": 401,
    "error": "Unauthorized",
    "message": "Invalid or expired token",
    "path": "/api/v1/users/booking/update/{id}"
````
### Delete Booking

#### Description
Delete Booking

#### HTTP request
````
POST /api/v1/users/booking/delete/{id}

Content-Type: application/json
Required token: yes
````

#### HTTP body request params
````
    /api/v1/users/booking/delete/1
````

#### Sample server response
````
    200 OK
````

#### Errors
````
    "timestamp": "2020-12-05T14:50:39.258250600",
    "status": 404,
    "error": "Not Found",
    "message": "Booking not found",
    "path": "/api/v1/users/booking/delete/{id}"

    "timestamp": "2020-12-05T15:04:44.037232100",
    "status": 401,
    "error": "Unauthorized",
    "message": "Invalid or expired token",
    "path": "/api/v1/users/booking/delete/{id}"
````