package gexabyte.project.hotel.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "hotels")
public class Hotel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false, name = "number_of_stars")
    private int starsNumber;

    @JsonIgnore
    @OneToMany(mappedBy = "hotels", fetch = FetchType.EAGER)
    private List<Room> rooms;
}

