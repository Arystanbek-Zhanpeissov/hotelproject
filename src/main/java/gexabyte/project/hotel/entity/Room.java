package gexabyte.project.hotel.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
@Table(name = "rooms")
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private BigDecimal price;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinTable(name = "rooms_types",
            joinColumns = @JoinColumn(
                    name = "room_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "type_id", referencedColumnName = "id"))
    private Type types;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "hotel_id")
    private Hotel hotels;
}

