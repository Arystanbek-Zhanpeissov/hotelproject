package gexabyte.project.hotel.controller;

import gexabyte.project.hotel.dto.AuthenticationDTO;
import gexabyte.project.hotel.dto.UserDTO;
import gexabyte.project.hotel.entity.User;
import gexabyte.project.hotel.exception.JwtAuthenticationException;
import gexabyte.project.hotel.security.jwt.JwtTokenProvider;
import gexabyte.project.hotel.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/v1/main/")
@Slf4j
public class MainController {

    @Value("${jwt.token.expired}")
    private long validityInMilliseconds;

    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;


    @Autowired
    public MainController(UserService userService,
                          AuthenticationManager authenticationManager,
                          JwtTokenProvider jwtTokenProvider) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;

    }

    @PostMapping("/registration")
    public ResponseEntity<?> createUser(@RequestBody UserDTO userDTO) {
        userService.createUser(userDTO);
        log.info("Registration complite successfully");
        return new ResponseEntity<>("Registration complite successfully",HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody AuthenticationDTO requestDto){
        try {
            String email = requestDto.getEmail();
            authenticationManager.authenticate
                    (new UsernamePasswordAuthenticationToken(email, requestDto.getPassword()));

            User user = userService.findUserByEmail(email);
            if (user == null) {
                throw new UsernameNotFoundException("User with username: " + email + " not found");
            }
            String token = jwtTokenProvider.createToken(email, user.getRoles(), validityInMilliseconds);
            Map<Object, Object> response = new HashMap<>();
            response.put("Message", "Login complite successfully");
            response.put("Username", email);
            response.put("Token", token);
            log.info("Login complite successfully");
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            throw new JwtAuthenticationException("Invalid username or password");
        }
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<?> getUserById(@PathVariable(name = "id") Long id) {
        User user = userService.findUserById(id);
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        UserDTO result = UserDTO.fromUser(user);
        log.info("Info about user = {}", user.getEmail());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
