## API documentation for hotel application
## Main controller
### Table of contents

- [Create admin](#create-admin)
- [Update user by ID](#update-user-by-id)
- [Delete user by ID](#delete-user-by-id)

- [Create Hotel](#create-hotel)
- [Update Hotel by ID](#update-hotel)
- [Delete Hotel by ID](#delete-hotel)

- [Create Room](#create-room)
- [Update Room by ID](#update-room)
- [Delete Room by ID](#delete-room)

- [Create Kitchen](#create-kitchen)
- [Create Order](#create-order)
- [Update Kitchen by ID](#update-kitchen)
- [Delete Kitchen by ID](#delete-kitchen)

### Create admin

#### Description
Create admin

#### HTTP request

````
POST /api/v1/admin/create

Content-Type: application/json
Required token: yes
````

#### HTTP body request params
````
{
    "firstName": //String
    "lastName": //String
    "email": //String
    "money": //int
    "password": //String
    "confirmPassword": //String
}
````

#### Sample body request
````
{
    "firstName":"user",
    "lastName":"user",
    "email":"user@mail.com",
    "money":5000,
    "password":"user",
    "confirmPassword":"user"
}
````

#### Sample server response
````
    200 OK
````

#### Errors

````
    "timestamp": "2020-12-05T14:09:50.220546500",
    "status": 400,
    "error": "Bad Request",
    "message": "Email in invalid",
    "path": "/api/v1/admin/create"

    "timestamp": "2020-12-05T14:11:17.159078200",
    "status": 400,
    "error": "Bad Request",
    "message": "Money must be greater then 0",
    "path": "/api/v1/admin/create"

    "timestamp": "2020-12-05T14:11:41.726056700",
    "status": 400,
    "error": "Bad Request",
    "message": "Passwords not equal",
    "path": "/api/v1/admin/create"

    "timestamp": "2020-12-05T15:04:44.037232100",
    "status": 401,
    "error": "Unauthorized",
    "message": "Invalid or expired token",
    "path": "/api/v1/admin/create"
````

### Update user by ID

#### Description
Update user by ID

#### HTTP request

````
POST /api/v1/admin/user/update/{id}

Content-Type: application/json
Required token: yes
````

#### HTTP body request params
````
{
    "firstName": //String,
    "lastName": //String,
    "email": //String,
    "money": int,
    "password": //String,
    "confirmPassword": //String
}
````

#### Sample body request
````
{
    "firstName":"user",
    "lastName":"user",
    "email":"user@mail.com",
    "money":5000,
    "password":"user",
    "confirmPassword":"user"
}
````

#### Sample server response
````
    200 OK
````

#### Errors

````
    "timestamp": "2020-12-05T15:23:54.022333300",
    "status": 404,
    "error": "Not Found",
    "message": "User not found",
    "path": "/api/v1/admin/user/update/{id}"

    "timestamp": "2020-12-05T14:09:50.220546500",
    "status": 400,
    "error": "Bad Request",
    "message": "Email in invalid",
    "path": "/api/v1/admin/update/{id}"

    "timestamp": "2020-12-05T14:11:17.159078200",
    "status": 400,
    "error": "Bad Request",
    "message": "Money must be greater then 0",
    "path": "/api/v1/admin/update/{id}"

    "timestamp": "2020-12-05T14:11:41.726056700",
    "status": 400,
    "error": "Bad Request",
    "message": "Passwords not equal",
    "path": "/api/v1/admin/update/{id}"

    "timestamp": "2020-12-05T15:04:44.037232100",
    "status": 401,
    "error": "Unauthorized",
    "message": "Invalid or expired token",
    "path": "/api/v1/admin/update/{id}"
````

### Delete user by ID

#### Description
Delete user by ID

#### HTTP request

````
POST /api/v1/admin/user/delete/{id}

Content-Type: application/json
Required token: yes
````

#### HTTP request params
````
    /api/v1/admin/user/delete/1
````

#### Sample server response
````
    200 OK
````

#### Errors

````
    "timestamp": "2020-12-05T15:04:44.037232100",
    "status": 401,
    "error": "Unauthorized",
    "message": "Invalid or expired token",
    "path": "/api/v1/admin/delete/{id}"

    "timestamp": "2020-12-10T10:52:28.338165600",
    "status": 404,
    "error": "Not Found"
    "message": "User not found",
    "path": "/api/v1/admin/user/delete/{id}"
````



### Create Hotel

#### Description
Create Hotel

#### HTTP request

````
POST /api/v1/admin/hotel/create

Content-Type: application/json
Required token: yes
````

#### HTTP body request params
````
{
    "name": //String,
    "starsNumber": //int
}
````

#### Sample body request
````
{
    "name": Hotel1,
    "starsNumber": 5
}
````

#### Sample server response
````
    201 Created
````

#### Errors
````
    "timestamp": "2020-12-05T15:04:44.037232100",
    "status": 401,
    "error": "Unauthorized",
    "message": "Invalid or expired token",
    "path": "/api/v1/admin/hotel/create"

    "timestamp": "2020-12-09T15:12:52.638496400",
    "status": 400,
    "error": "Bad Request",
    "message": "Enter stars number greater or equal 1",
    "path": "/api/v1/admin/hotel/create"

    "timestamp": "2020-12-09T15:13:26.011494700",
    "status": 400,
    "error": "Bad Request",
    "message": "Enter stars number less or equal 5",
    "path": "/api/v1/admin/hotel/create"    
````
### Update Hotel

#### Description
Update Hotel

#### HTTP request
````
POST /api/v1/admin/hotel/update/{id}

Content-Type: application/json
Required token: yes
````

#### HTTP body request params
````
{
    "name": //String,
    "starsNumber": //int
}
````

#### Sample body request
````
{
    "name": Hotel1,
    "starsNumber": 5
}
````

#### Sample server response
````
    200 OK
````

#### Errors

````
    "timestamp": "2020-12-05T15:04:44.037232100",
    "status": 401,
    "error": "Unauthorized",
    "message": "Invalid or expired token",
    "path": "/api/v1/admin/hotel/update/{id}"

    "timestamp": "2020-12-05T15:11:41.527163300",
    "status": 404,
    "error": "Not Found",
    "message": "Hotel not found",
    "path": "/api/v1/admin/hotel/update/{id}"

    "timestamp": "2020-12-09T15:12:52.638496400",
    "status": 400,
    "error": "Bad Request",
    "message": "Enter stars number greater or equal 1",
    "path": "/api/v1/admin/hotel/update/{id}"

    "timestamp": "2020-12-09T15:13:26.011494700",
    "status": 400,
    "error": "Bad Request",
    "message": "Enter stars number less or equal 5",
    "path": "/api/v1/admin/hotel/update/{id}"  
````
### Delete Hotel

#### Description
Delete Hotel

#### HTTP request

````
POST /api/v1/admin/hotel/delete/{id}

Content-Type: application/json
Required token: yes
````

#### HTTP request params
````
    /api/v1/admin/hotel/delete/1
````

#### Sample server response
````
    200 OK
````

#### Errors

````
    "timestamp": "2020-12-05T15:04:44.037232100",
    "status": 401,
    "error": "Unauthorized",
    "message": "Invalid or expired token",
    "path": "/api/v1/admin/hotel/update/{id}"
    
    "timestamp": "2020-12-09T15:28:28.583221500",
    "status": 404,
    "error": "Not Found",
    "message": "Hotel not found",
    "path": "/api/v1/admin/hotel/delete/{id}"
````
### Create Room

#### Description
Create Room

#### HTTP request

````
POST /api/v1/admin/room/create

Content-Type: application/json
Required token: yes
````

#### HTTP body request params
````
{
    "price": //int,
    "typeId": //int,
    "hotelId": //int
}
````

#### Sample body request
````
{
    "price":500,
    "typeId":1,
    "hotelId":1
}
````

#### Sample server response
````
    200 OK
````

#### Errors
````
    "timestamp": "2020-12-05T15:13:32.916902700",
    "status": 404,
    "error": "Not Found",
    "message": "Hotel not found",
    "path": "/api/v1/admin/room/create"
    
    "timestamp": "2020-12-05T15:13:50.844642",
    "status": 404,
    "error": "Not Found",
    "message": "Type not found",
    "path": "/api/v1/admin/room/create"
    
    "timestamp": "2020-12-05T15:04:44.037232100",
    "status": 401,
    "error": "Unauthorized",
    "message": "Invalid or expired token",
    "path": "/api/v1/admin/room/create"
````
### Update Room

#### Description
Update Room

#### HTTP request

````
POST /api/v1/admin/room/update/{id}

Content-Type: application/json
Required token: yes
````

#### HTTP body request params
````
{
    "price": //int,
    "typeId": //int,
    "hotelId": //int
}
````

#### Sample body request
````
{
    "price":500,
    "typeId":1,
    "hotelId":1
}
````

#### Sample server response
````
    200 OK
````

#### Errors

````
    "timestamp": "2020-12-05T15:13:32.916902700",
    "status": 404,
    "error": "Not Found",
    "message": "Hotel not found",
    "path": "/api/v1/admin/room/update/{id}"
    
    "timestamp": "2020-12-05T15:13:50.844642",
    "status": 404,
    "error": "Not Found",
    "message": "Type not found",
    "path": "/api/v1/admin/room/update/{id}"
    
    "timestamp": "2020-12-05T15:04:44.037232100",
    "status": 401,
    "error": "Unauthorized",
    "message": "Invalid or expired token",
    "path": "/api/v1/admin/room/update/{id}"

    "timestamp": "2020-12-09T16:42:55.964997200",
    "status": 404,
    "error": "Not Found",
    "message": "Room not found",
    "path": "/api/v1/admin/room/update/{id}"
````
### Delete Room

#### Description
Delete Room

#### HTTP request

````
POST /api/v1/admin/room/delete/{id}

Content-Type: application/json
Required token: yes
````

#### HTTP request params
````
    /api/v1/admin/room/delete/1
````

#### Sample server response
````
    200 OK
````

#### Errors

````
    "timestamp": "2020-12-09T16:42:55.964997200",
    "status": 404,
    "error": "Not Found",
    "message": "Room not found",
    "path": "/api/v1/admin/room/delete/{id}"

    "timestamp": "2020-12-05T15:04:44.037232100",
    "status": 401,
    "error": "Unauthorized",
    "message": "Invalid or expired token",
    "path": "/api/v1/admin/room/delete/{id}"
````
### Create Kitchen

#### Description
Create Kitchen

#### HTTP request

````
POST /api/v1/admin/kitchen/create

Content-Type: application/json
Required token: yes
````

#### HTTP body request params
````
{
    "foodName": //String,
    "price": //int,
    "hotelId": //int
}
````

#### Sample body request
````
{
    "foodName":"Apple",
    "price":300,
    "hotelId": 1
}
````

#### Errors
````
    "timestamp": "2020-12-05T15:18:34.751538400",
    "status": 404,
    "error": "Not Found",
    "message": "Hotel not found",
    "path": "/api/v1/admin/kitchen/create"

    "timestamp": "2020-12-05T15:04:44.037232100",
    "status": 401,
    "error": "Unauthorized",
    "message": "Invalid or expired token",
    "path": "/api/v1/admin/kitchen/create"
````
### Create Order

#### Description
Create Order

#### HTTP request

````
POST /api/v1/admin/kitchen/create/order

Content-Type: application/json
Required token: yes
````

#### HTTP body request params
````
{
    "foodName": //String,
    "price": //int,
    "hotelId": //int
}
````

#### Sample body request
````
{
    "foodName":"Banana",
    "price":300,
    "hotelId": 1
}
````

#### Sample server response
````
    200 OK
````

#### Errors

````
    "timestamp": "2020-12-05T15:17:10.159700",
    "status": 404,
    "error": "Not Found",
    "message": "Hotel not found",
    "path": "/api/v1/admin/kitchen/create/order"

    "timestamp": "2020-12-05T15:04:44.037232100",
    "status": 401,
    "error": "Unauthorized",
    "message": "Invalid or expired token",
    "path": "/api/v1/admin/kitchen/create/order"
````

### Update Kitchen

#### Description
Update Kitchen

#### HTTP request

````
POST /api/v1/admin/kitchen/update/{id}

Content-Type: application/json
Required token: yes
````

#### HTTP body request params
````
{
    "foodName": //String,
    "price": //int,
    "hotelId": //int
}
````

#### Sample body request
````
{
    "foodName":"Banana",
    "price":300,
    "hotelId": 1
}
````

#### Sample server response
````
    200 OK
````

#### Errors

````
    "timestamp": "2020-12-05T15:19:35.574698300",
    "status": 404,
    "error": "Not Found",
    "message": "Hotel not found",
    "path": "/api/v1/admin/kitchen/update/{id}"

    "timestamp": "2020-12-05T15:04:44.037232100",
    "status": 401,
    "error": "Unauthorized",
    "message": "Invalid or expired token",
    "path": "/api/v1/admin/kitchen/update/{id}"
````
### Delete Kitchen

#### Description
Delete Kitchen

#### HTTP request

````
POST /api/v1/admin/kitchen/delete/{id}

Content-Type: application/json
Required token: yes
````

#### HTTP request params
````
    /api/v1/admin/kitchen/delete/1
````

#### Sample server response
````
    200 OK
````

#### Errors

````
    "timestamp": "2020-12-05T15:04:44.037232100",
    "status": 401,
    "error": "Unauthorized",
    "message": "Invalid or expired token",
    "path": "/api/v1/admin/kitchen/delete/{id}"

    "timestamp": "2020-12-09T16:51:27.988072500",
    "status": 404,
    "error": "Not Found",
    "message": "Kitchen not found",
    "path": "/api/v1/admin/kitchen/delete/{id}"
````



