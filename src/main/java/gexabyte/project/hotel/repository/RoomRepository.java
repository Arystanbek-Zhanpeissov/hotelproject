package gexabyte.project.hotel.repository;


import gexabyte.project.hotel.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoomRepository extends JpaRepository<Room, Long> {
    Room findByTypes_Name(String name);
    boolean existsById(Long id);
}
