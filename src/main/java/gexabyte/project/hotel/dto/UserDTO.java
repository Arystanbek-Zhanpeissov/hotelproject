package gexabyte.project.hotel.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gexabyte.project.hotel.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDTO {
    private String firstName;
    private String lastName;
    private BigDecimal money;
    private String email;
    private String password;
    private String confirmPassword;
    private Long roles;

    public static UserDTO fromUser(User user) {
        UserDTO userDto = new UserDTO();
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setMoney(user.getMoney());
        userDto.setEmail(user.getEmail());
        userDto.setPassword(user.getPassword());
        return userDto;
    }
}
