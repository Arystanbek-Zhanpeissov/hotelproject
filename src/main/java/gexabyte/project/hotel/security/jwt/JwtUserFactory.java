package gexabyte.project.hotel.security.jwt;

import gexabyte.project.hotel.entity.Role;
import gexabyte.project.hotel.entity.User;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.List;

public class JwtUserFactory {

    public JwtUserFactory() {
    }

    public static JwtUser create(User user) {
        return new JwtUser(
                user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getEmail(),
                user.getPassword(),
                user,
                mapToGrantedAuthorities(user.getRoles())
        );
    }
    private static List<SimpleGrantedAuthority> mapToGrantedAuthorities(Role role) {
        List<SimpleGrantedAuthority> permissions = new ArrayList<>();
        permissions.add(new SimpleGrantedAuthority(role.getName()));
        return permissions;
    }

}
